C code for a quick numerical simulation of the behaviour of spin polarized muons in an external magnetic field that both fluctuates and has a spatially random component. 

Code is in Code/

Compile with:

gcc -Wall -W -lm -O2 main.c expdev.c normdev.c progress.c ensblav.c evolve.c -o hopping

Run with:

./hopping output.dat

Reasonable values to get a quick output are:
Magnetic field Bx=0, By=0, Bz=1; 
Number of muons N=10000; 
Correlation time tau_c=1; 
Clock period ts=0.1; 
Number of temporal iterations T: 1000
