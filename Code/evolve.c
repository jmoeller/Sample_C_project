//evolve.c 
// Evolve rho(t) by dt

#include <math.h>
#include <stdlib.h>
#include <complex.h>
#include <time.h>
#include "declarations.h"

void evolve(double complex rho_new[MSIZE][MSIZE], double complex rho_old[MSIZE][MSIZE], double complex U[MSIZE][MSIZE], double complex U_dag[MSIZE][MSIZE], double bx, double by, double bz, double w, double dt)
{

  int i, j, k, l; 
  
  // Calculate differential time evolution operator U(dt) and Hermitian conjugate U_dag(dt)
  
  U[0][0] = cos(w*dt/2) - I*bz*sin(w*dt/2);
  U[0][1] = -(by+I*bx) * sin(w*dt/2);
  U[1][0] = (by - I*bx) * sin(w*dt/2);
  U[1][1] = cos(w*dt/2) + I*bz*sin(w*dt/2);
  
  // Remember to transpose as well! Argh!
  U_dag[0][0] = cos(w*dt/2) + I*bz*sin(w*dt/2);
  U_dag[0][1] = (by + I*bx) * sin(w*dt/2);
  U_dag[1][0] = (-by+I*bx) * sin(w*dt/2);
  U_dag[1][1] = cos(w*dt/2) - I*bz*sin(w*dt/2);
  
  for(i=0; i<=1; i++)
    {
      for(l=0; l<=1; l++)
	{      
	  
	  rho_new[i][l] = 0; /* Don't forget to set it to 0 before the summation */
	  
	  for(k=0; k<=1; k++)
	    {
	      for(j=0; j<=1; j++)
		{
		  rho_new[i][l] += U_dag[i][j] * rho_old[j][k] * U[k][l];
		}
	    }
	}
    }
  
  
  // Update rho_old[i][j] for next loop
  
  for(i=0; i<=1; i++)
    {
      for(j=0; j<=1; j++)
	{
	  rho_old[i][j] = rho_new[i][j]; /* And not the other way around */
	}
    }
  
  return ;
}
