// progress.c
// Program to print out current progress to screen

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <complex.h>
#include "declarations.h"

void progress(int mu, int N, time_t start, time_t current)
{

  double prog, step;
  double limit = 0.0;
  double t_rem;

  step = 100 * 1/((double) N); // 100 * to get it in %
  prog = 100 * ((double) mu + 1.0)/ ((double) N); // Remember mu starts counting from 0, so add 1.0

  t_rem = difftime(current, start) * ((double) N / ((double) mu + 1.0) - 1.0);

  while(limit <= 100.0)
    {
      if((prog>=(limit-0.5*step)) && (prog<(limit+0.5*step)))
	printf("----- Progress: %3.0lf %% ----- || ----- Est. time remaining: %4.0lf s -----\n", limit, t_rem); // %% prints % sign
      limit += 1.0;
    }
      
  return ;
}

