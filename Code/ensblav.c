// ensblav.c
// Computes the ensemble average from Si, rho_new and N

#include <stdlib.h>
#include <complex.h>
#include <time.h>
#include "declarations.h"

double ensblav(double complex Si[MSIZE][MSIZE], double complex rho_new[MSIZE][MSIZE], int N)
{
  
  int i, j, k;
  
  double complex Sirho[MSIZE][MSIZE]; // Products of matrices Si and rho
  
  double Si_av = 0; // Ensemble average at time of program call
  
  // Calculate product of matrix Si and rho(t) 
  
  for(i=0; i<=1; i++)
    {
      for(k=0; k<=1; k++)
	{      
	  
	  Sirho[i][k] = 0; /*Don't forget to set it to 0 before the summation!!*/
	  
	  for(j=0; j<=1; j++)
	    {
	      Sirho[i][k] += Si[i][j] * rho_new[j][k]; 
	    }
	}
    }
  
  
  // Calculate final ensemble average [Si]=Tr(Si.rho(t)) by summing and dividing by N
  
  for(i=0; i<=1; i++)
    {
      Si_av += Sirho[i][i] / N; 
    }
  
  return Si_av;
}
