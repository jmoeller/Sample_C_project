// normdev.c
// Uses Box-Muller method to generate random deviates with a normal (Gaussian) distribution centred at mean and of standard deviation stddev - both of which need to be specified upon calling it, and returns them
// Note that the ratio of uniforms method is significantly faster
// Needs random number generator to be seeded once and only once before called in int main()

#include <math.h>
#include <stdlib.h>
#include <complex.h>
#include <time.h>
#include "declarations.h"

double normdev(double mean, double stddev) // Function definition
{
  static int iset = 0; // Flag to indicate whether a random Gaussian deviate gset is still in memory
  static double gset; // Gaussian deviate to be returned if iset == 1, ie. the one in memory
  double fac, rsq, v1, v2; // fac is an intermediate variable, basically the common factor in y1 and y2 in the Box-Muller transformation, rsq is the squared length of the vector (v1,v2), v1 and v2 are uniform deviates from [-1,1]

  if (iset == 0) // If there is no extra random deviate still in memory
    {
      do // At least once execute the following
	{
	  v1 = 2.0 * drand48() - 1; // Generate two random doubles from [-1;1]
	  v2 = 2.0 * drand48() - 1; 
	  rsq = v1 * v1 + v2 * v2; // Calculate length of vector (v1,v2)
	} 
      while(rsq >= 1.0 || rsq ==0.0); // Keep doing this until (v1,v2) lies within the unit circle and is not (0,0)
      
      fac = sqrt( -2.0 * log(rsq) / rsq); // Make Box-Muller transformation
      gset = v1 * fac; // Set gset to y1
      iset = 1; // Set flag to 1 indicating another Gaussian deviate in memory
      return mean + stddev * v2 * fac; // And return y2
    }
  else
    {
      iset = 0; // So a new pair of Gaussian random deviates in generated on the next run
      return mean + stddev * gset; // Return y1 (from memory)
    }
}
