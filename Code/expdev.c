// expdev.c
// Generates exponentially distributed random numbers

#include <math.h>
#include <stdlib.h>
#include <complex.h>
#include <time.h>
#include "declarations.h"

double expdev(double tau_c)
{

  return - tau_c * log(1 - drand48());

}
