// main.c for project 4. Hopping v4. Uses Box-Muller method for generating normal deviates
// Uses natural units
// gcc -Wall -W -lm -O2 main.c expdev.c normdev.c progress.c ensblav.c evolve.c -o hopping

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <complex.h>
#include "declarations.h"

int main(int argc, char *argv[])
{

  //
  // Declare variables
  //

  int mu, N, t, T; // mu: Muon iterator; N: Number of muons to average over; t: Time iterator - careful this is an integer iterator now, T: Number of temporal iterations

  double complex Sx[MSIZE][MSIZE] = {{0,1},{1,0}}, Sy[MSIZE][MSIZE] = {{0,-1*I},{1*I,0}}, Sz[MSIZE][MSIZE] = {{1,0},{0,-1}}; // Actually Pauli sigma matrices not spin matrices (which are h_bar/2 * these), this is to normalise the polarisation to 1

  double *Sx_av, *Sy_av, *Sz_av; // Final ensemble average polarisation at time t, form is Si_av[t]

  double complex rho_old[MSIZE][MSIZE], rho_new[MSIZE][MSIZE]; // Will iterate by dt using differential time evolution operators, old is the old rho before iteration by one step, new is the new rho after each step

  double complex U[MSIZE][MSIZE]={{0,0},{0,0}}, U_dag[MSIZE][MSIZE]={{0,0},{0,0}}; // Time evolution operator and Hermitian conjugate

  double B, Bx, By, Bz, bx, by, bz; // B vector and normal vector along it (bx,by,bz)

  double w, Delta, gamma = 850.623748; // Units of w = eB/(m_mu) [SI - not Gaussian!] are Mhz if B is in T, units of gamma are Mhz/T

  double t_nat, tmax, dt; // All times are in natural units of 1/Delta where Delta = gamma * stddev
 
  double meanx, meany, meanz, stddev = 1.0; // meani is the mean of the Gaussian distribution in i direction, stddev is taken to be the same in all directions, it is fixed in natural units

  double tau_left, tau_c, ts_left, ts; // All in natural units of 1/Delta

  time_t start, current, stop;

  FILE *fout;

  // Check whether file is accessible
  //

  if(argc>=2) // Use entered file name 
    {
      if ((fout = fopen(argv[1], "w")) == NULL)
	{
	  printf("Cannot open file '%s'\n", argv[1]);
	  exit(1);
	}
    }
  else // Otherwise use default file name 
    {
      if ((fout = fopen("output.dat", "w")) == NULL)
	{
	  printf("Cannot open file 'output.dat'\n");
	  exit(1);
	}
    }
  
  //  
  // Get input
  //

  printf("4. Muon hopping v4. Uses Box-Muller method to compute normal deviates. Uses natural units. \n");
  printf("-----------------------------------------------------------\n");
  
  printf("Syntax is: ./hopping filename.extension (default: output.dat)\n");  

  printf("Enter external field in x direction (in units of the internal B stddev): ");
  scanf("%lf", &meanx);

  printf("Enter external field in y direction (in units of the internal B stddev): ");
  scanf("%lf", &meany);

  printf("Enter external field in z direction (in units of the internal B stddev): ");
  scanf("%lf", &meanz);
  
  printf("Enter number of muons N you want to average over: ");
  scanf("%d", &N);

  printf("Enter correlation time tau_c (in units of 1/Delta): ");
  scanf("%lf", &tau_c);

  printf("Enter clock period ts (in units of 1/Delta): ");
  scanf("%lf", &ts);

  printf("Enter number of temporal iterations T: ");
  scanf("%d", &T);


  // Calculate Delta and tmax
  Delta = stddev * gamma; // Inverse unit of time here

  tmax = ts * T; // In units of 1/Delta


  // Get time and print time stamp to screen and file
  time(&start);
  printf("Local time and date at start of main computation: %.24s \n", asctime (localtime( &start))); 
  
  fprintf(fout, "# Data for 4. Muon hopping v4. \n"); // # comments out for gnuplot  
  fprintf(fout, "# Local time and date at start of main computation: %.24s \n", asctime (localtime( &start)));
  
  //
  // Allocate memory for arrays
  //

  // Sx_av
  
  Sx_av =  malloc( T * sizeof(double) ); 
  
  //DO CHECK IF ALLOCATION WAS SUCCESSFUL EACH TIME!
  if(Sx_av==NULL) 
    {
      printf("ERROR! Cannot allocate memory!\n");
      exit(1);
    }

  // Sy_av
  
  Sy_av =  malloc( T * sizeof(double) ); 
  
  //DO CHECK IF ALLOCATION WAS SUCCESSFUL EACH TIME!
  if(Sy_av==NULL) 
    {
      printf("ERROR! Cannot allocate memory!\n");
      exit(1);
    }

  // Sz_av
  
  Sz_av =  malloc( T * sizeof(double) ); 
  
  //DO CHECK IF ALLOCATION WAS SUCCESSFUL EACH TIME!
  if(Sz_av==NULL) 
    {
      printf("ERROR! Cannot allocate memory!\n");
      exit(1);
    }
  

  // Seed random number generator
  srand48((unsigned int) time(NULL)); 
  
  //
  // Start to loop for different muons
  // Take care not to write beyond limits of an array
  
  // Reset final ensemble average to 0 before summation
  for(t=0; t<T; t++)
    {
      Sx_av[t] = 0; 
      Sy_av[t] = 0;
      Sz_av[t] = 0; 
    }
  

  for(mu=0; mu<N; mu++) 
    {
      
      // Must reset rho_old every time as we loop over many muons!
      rho_old[0][0] = 1.0;
      rho_old[0][1] = 0.0;
      rho_old[1][0] = 0.0;
      rho_old[1][1] = 0.0;     
           
      // Reset time counter
      t = 0;     

      //
      // Begin temporal loop
      //
      
      // Initialise	
      //
      
      // Get tau_left
      tau_left = expdev(tau_c);
      
      // Set ts_left
      ts_left = ts;
      
      // Get B
      
      Bx = normdev(meanx, stddev);
      By = normdev(meany, stddev);
      Bz = normdev(meanz, stddev);
      
      B = sqrt(Bx * Bx + By * By + Bz * Bz);
      
      w = B / stddev; // This is in natural units, w is measured in units of Delta
      
      // Compute normalised normal vector along B      
      bx = Bx / B;
      by = By / B;
      bz = Bz / B;

      while(t<T) 
      {
	
	if(tau_left >= ts_left)
	  {
	    // Evolve rho by dt = ts_left
	    // 
	    
	    dt = ts_left;
	    
	    evolve(rho_new, rho_old, U, U_dag, bx, by, bz, w, dt);

	    // And compute ensemble average
	    //

	    Sx_av[t] += ensblav(Sx, rho_new, N);
	    Sy_av[t] += ensblav(Sy, rho_new, N);
	    Sz_av[t] += ensblav(Sz, rho_new, N);

	    // Finally increment time iterator t as we've proceeded to the next clock time
	    t ++;

	    // Update tau_left
	    tau_left -= ts_left;

	    // Update ts_left
	    ts_left = ts;
	    
	  } // Closes if(tau_left >= ts_left) 
	
	else if((tau_left < ts_left) && (tau_left >=0.0))
	  {
	    
	    // Evolve rho by dt = tau_left
	    // 

	    dt = tau_left;
	    
	    evolve(rho_new, rho_old, U, U_dag, bx, by, bz, w, dt);

	    // Update ts_left
	    ts_left -= tau_left;	    

	    // Get new tau_left
	    tau_left = expdev(tau_c);

	    // Get new B

	    Bx = normdev(meanx, stddev);
	    By = normdev(meany, stddev);
	    Bz = normdev(meanz, stddev);
	    
	    B = sqrt(Bx * Bx + By * By + Bz * Bz);
	    
	    w = B / stddev; // This is in natural units, w is measured in units of Delta
	    
	    // Compute normalised normal vector along B      
	    bx = Bx / B;
	    by = By / B;
	    bz = Bz / B;
	    
	  } // Closes if(tau_left < ts_left)

	else
	  {
	    printf("ERROR! Computational Error in comparison of tau_left and ts_left.");
	    exit(1);
	  }

	   
	} // Closes temporal iteration for loop

      
      // Get time
      time(&current);
      
      progress(mu, N, start, current); // Prints progress to screen and estimates remaining time
      
    }  // Closes muon loop
  
  // Get time
  time(&stop);

  // Print time to screen
  printf("Local time and date after main computation: %.24s \n", asctime (localtime( &stop)));
  printf("Approximate time elapsed during main computation: %.0lf s, of which CPU time: %.2lf s \n", difftime(stop, start), (double) clock() / CLOCKS_PER_SEC);
  

  // Print output to file

  fprintf(fout, "# Local time and date after main computation: %.24s \n", asctime (localtime( &stop)));
  fprintf(fout, "# Approximate time elapsed during main computation: %.0lf s, of which CPU time: %.2lf s \n", difftime(stop, start), (double) clock() / CLOCKS_PER_SEC);
  
  fprintf(fout, "# ---------------------------------------------------------------------------------------------------\n");
  fprintf(fout, "# All values are in natural units. stddev is the standard deviation of the internal field. Delta = gamma * stddev = %lf Mhz. For the computation we've chosen stddev = %lf T. External fields: Bx = %lf stddev = %lf T, By = %lf stddev = %lf T, Bz = %lf stddev = %lf T. Number of muons to average over N = %d. Correlation time tau_c = %16.15lf 1/Delta = %16.15lf mu s. Clock period ts = %16.15lf 1/Delta = %16.15lf mu s. After each period write current Si_av to array Si_av[t]. Number of temporal iterations T = %d, which means tmax = %16.15lf 1/Delta = %16.15lf mu s. \n", Delta, stddev, meanx, meanx * stddev, meany, meany * stddev, meanz, meanz * stddev, N, tau_c, tau_c / Delta, ts, ts / Delta, T, tmax, tmax / Delta);
  
  fprintf(fout, "# t[1/Delta] \t\t t[mu s] \t\t\t [Sx] \t\t\t [Sy] \t\t\t [Sz]\n");
  fprintf(fout, "# -------------------------------------------------------------------------------------------------------------------\n");
  
  for(t=0; t<T; t++) // Again, the last element of array is Sx_av[T-1], don't write beyond it! Otherwise get segfault.
    {

      t_nat = t * ts; // Computes time in natural units
      fprintf(fout, "%.15lf \t %.15lf \t %.15lf \t %.15lf \t %.15lf\n", t_nat, t_nat / Delta, Sx_av[t], Sy_av[t], Sz_av[t]); 
      
    }

  // ALWAYS FREE THE MEMORY IN THE END 
  
  free(Sx_av);
  free(Sy_av);
  free(Sz_av);

  fclose(fout); /* Closes file */
  return 0;
}
