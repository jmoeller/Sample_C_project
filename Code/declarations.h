// declarations.h

// Get exponentially distributed deviate, tau_c = correlation time, usually this is all in mu s
// Requires random number generator to be seeded once and only once before called in int main()
// Depends on complex.h, must be included everywhere before this
double expdev(double tau_c);

// Generate Gaussian random numbers
// Requires random number generator to be seeded once and only once before called in int main()
double normdev(double mean, double stddev); 

// Function to print current progress to the screen and estimate remaining time
// Depends on time.h
void progress(int mu, int N, time_t start, time_t current);

// Define MATSIZE for matrix multiplications
#define MSIZE 2

// Calculates trace of product of 2x2 matrices Si and rho_new which can have complex components but the trace must be real
double ensblav(double complex Si[MSIZE][MSIZE], double complex rho_new[MSIZE][MSIZE], int N);

// Evole rho
void evolve(double complex rho_new[MSIZE][MSIZE], double complex rho_old[MSIZE][MSIZE], double complex U[MSIZE][MSIZE], double complex U_dag[MSIZE][MSIZE], double bx, double by, double bz, double w, double dt);
